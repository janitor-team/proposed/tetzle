# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Graeme Gott
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: Tetzle\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-11-14 20:57+0000\n"
"PO-Revision-Date: 2018-05-28 19:03+0000\n"
"Last-Translator: Graeme Gott <graeme@gottcode.org>\n"
"Language-Team: Portuguese (Brazil) (http://www.transifex.com/gottcode/tetzle/"
"language/pt_BR/)\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: ../tetzle.appdata.xml.in:6 ../tetzle.desktop.in:4
msgid "Tetzle"
msgstr "Tetzle"

#: ../tetzle.appdata.xml.in:7 ../tetzle.desktop.in:6
msgid "Jigsaw puzzle with tetromino pieces"
msgstr "Quebra-cabeças com peças em formato de tetris"

#: ../tetzle.appdata.xml.in:10
msgid ""
"Tetzle is a jigsaw puzzle game that uses tetrominoes for the pieces. Any "
"image can be imported and used to create puzzles with a wide range of sizes. "
"Games are saved automatically, and you can select between currently in "
"progress games."
msgstr ""

#: ../tetzle.desktop.in:5
msgid "Jigsaw Puzzle Game"
msgstr "Jogo de quebra-cabeças"
